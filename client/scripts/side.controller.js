angular
  .module('DawnFortApp')
  .controller('SideController', ['$scope', '$rootScope', '$location', '$http', function ($scope, $rootScope, $location, $http) {
    var vm = this;

    vm.view = {
      
    };

    $rootScope.$watch('user', function(val) {
      if (val === undefined || !val || !val.id) return;
      vm.user = val;
      if (!vm.view._experience) {
        vm.view._experience = vm.user.character.experience;
      } else {
        anime({
          targets: vm.view,
          _experience: vm.user.character.experience,
          duration: (vm.user.character.experience - vm.view._experience) * 50,
          easing: 'linear',
          round: 1,
          update: function() {
            $scope.$apply();
          }
        });
      }
      vm.currentLevelExp = getLevelExp(vm.user.character.level);
      vm.levelUpAvailable = (vm.user.character.experience / vm.currentLevelExp) > 1;
    });

    vm.goto = function(path) {
      $location.path(path);
    };

    var getLevelExp = function (level) {
      var exp = (level * 111);
      exp += parseInt(level / 10) * 333;
      exp += parseInt(level / 50) * 9999;
      exp += parseInt(level / 100) * 999999;
      return exp;
    };

    vm.levelUp = function() {
      $http.get('user/' + vm.user.id + '/levelup').then(function(response) {
        $rootScope.updateStatus();
      });
    };
  }]);