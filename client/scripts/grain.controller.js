angular
  .module('DawnFortApp')
  .controller('GrainController', ['$scope', '$location', '$route', '$rootScope', '$http', function ($scope, $location, $route, $rootScope, $http) {
    var vm = this;

    $rootScope.$watch('user', function (val, oldVal) {
      vm.user = val;
    });

    vm.view = {
      grainSelected: 1,
    };

    vm.sell = function() {
      if (parseInt(vm.view.grainSelected) > 0) {
        $http.get('sell-grain?grain=' + vm.view.grainSelected).then(function(response) {
          $rootScope.updateStatus();
          vm.view.successMessage = vm.view.failMessage = vm.view.infoMessage = '';
          if (response.data.status == 'ok') {
            vm.view.successMessage = response.data.message;
          } else if (response.data.status == 'info') {
            vm.view.infoMessage = response.data.message;
          } else if (response.data.status == 'error') {
            vm.view.failMessage = response.data.message;
          }
          vm.view.grainSelected = 0;
        });
      }
    };
  }]);