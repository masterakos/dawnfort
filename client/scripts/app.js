angular
  .module('DawnFortApp', [
    'ngRoute',
  ])
  .run(['$rootScope', function($rootScope) {
    $rootScope.isMobile = (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
  }])
  .run(['$http', '$location', '$rootScope', function($http, $location, $rootScope) {
    $http.get('status').then(function(response) {
      if (response.data.id) {
        $rootScope.user = response.data;
        if ($location.path() == '/') {
          $location.path('safehouse');
        }
      } else {
        $location.path('/');
      }
    });
  }]).filter('capitalize', function () {
    return function (input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
  }).filter('commafy', function () {
    return function (input) {
      if (!input) return input;
      return commafy(input);
    }
  })
  .filter('reverse', function () {
    return function (array) {
      return array.slice().reverse();
    }
  })
  .directive('ngEnter', function () {
    return function (scope, element, attrs) {
      element.bind("keydown keypress", function (event) {
        if (event.which === 13) {
          scope.$apply(function () {
            scope.$eval(attrs.ngEnter);
          });
          event.preventDefault();
        }
      });
    };
  })

function isNumeric(val) {
  if (typeof val === 'number' && !isNaN(val)) {
    return true;
  }

  val = (val || '').toString().trim();
  return val && !isNaN(val);
}

function commafy(val) {
  if (typeof val === 'undefined' || val === null) {
    val = '';
  }

  val = val.toString();

  if (!isNumeric(val)) {
    return val;
  }

  var parts = val.split('.');

  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  return parts.join('.');
}
