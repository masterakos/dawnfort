angular
  .module('DawnFortApp')
  .controller('ScavengeController', ['$scope', '$location', '$rootScope', function($scope, $location, $rootScope) {
    var vm = this;

    $rootScope.$watch('user', function(val) {
      vm.user = val;
    });
    
    vm.view = {
      areas: [
        {
          name: 'Restaurant',
          points: 1,
        },
        {
          name: 'Warehouse',
          points: 2,
        },
        {
          name: 'Shopping Mall',
          points: 3,
        },
        {
          name: 'College',
          points: 4,
        },
        {
          name: 'Arcade',
          points: 5,
        },
        {
          name: 'Cinema',
          points: 6,
        },
        {
          name: 'Hotel',
          points: 7,
        },
        {
          name: 'Bank',
          points: 8,
        },
        {
          name: 'Prison',
          points: 9,
        },
      ]
    };

    vm.goto = function(scavengeId) {
      $location.path('scavenge/' + scavengeId);
    };
  }]);