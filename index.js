var repl = require('repl');

var WebServer = require('./web-server');
var Database = require('./database');
var Game = require('./game');
var SocketServer = require('./socket-server');

(async function() {
  var database = new Database();
  await database.connect();

  var gameEngine = new Game(database);

  var webServer = new WebServer(database, gameEngine);
  var socketServer = new SocketServer(webServer.instance, gameEngine);
  // webServer.start(3000);

  repl.start('> ').context.game = {
    engine: gameEngine,
    wss: socketServer,
  }
})();